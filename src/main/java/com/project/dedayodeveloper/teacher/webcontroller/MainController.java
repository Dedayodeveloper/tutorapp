package com.project.dedayodeveloper.teacher.webcontroller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {


    @GetMapping
    public String index(){
        return "index";
    }
}
